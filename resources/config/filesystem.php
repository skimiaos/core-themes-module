<?php

return [
    'theme' => [
        'file' => [
            'info' =>'theme.info.php'
        ],
        'dir'=>[
            'views'=>'views',
            'assets'=>'assets'
        ]

    ],
    'themes' => [
        'dir' => [
            'path' => base_path().'/themes'
        ]
    ]
];