<?php

namespace Skimia\Themes;

use Skimia\Modules\ModuleBase;
use Theme;

class Module extends ModuleBase{

    public function preBoot(){

    }

    public function register(){
        parent::register();


        measure('Chargement des Themes', function() {
            Theme::loadThemes();


        });
    }

    public function beforeRegisterModules(){
        $paths = $this->app['config']['view.paths'];
        $finder = new View\FileViewFinder($this->app['files'], $paths);
        \View::setFinder($finder);
    }

    public function getAliases(){
        return [
            'Assets' => 'Skimia\Themes\Facades\Assets',
            'Theme' => 'Skimia\Themes\Facades\Theme',
        ];
    }

} 