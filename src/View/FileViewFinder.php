<?php
namespace Skimia\Themes\View;
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 13/11/2014
 * Time: 20:51
 */
use Illuminate\View\FileViewFinder as FileViewFinderBase;
use Theme;
use Config;
class FileViewFinder extends FileViewFinderBase{

    protected function findInPaths($name, $paths)
    {

        $paths = array_merge($paths,Theme::getIncludePaths());
        foreach ((array) $paths as $path)
        {
            foreach ($this->getPossibleViewFiles($name) as $file)
            {
                if ($this->files->exists($viewPath = $path.'/'.$file))
                {
                    return $viewPath;
                }
            }
        }

        throw new \InvalidArgumentException("View [$name] not found.");
    }

    protected function findNamedPathView($name)
    {
        list($namespace, $view) = $this->getNamespaceSegments($name);

        $app_path = array_map(function($value) use ($namespace){
            return $value .'/modules/'.str_replace('.','/',$namespace);
        },Config::get('view.paths'));

        $modules_paths = array_map(function($value) use ($namespace){
            return dirname($value) .'/modules/'.str_replace('.','/',$namespace);
        },Theme::getIncludePaths());

        $paths = array_merge($app_path, $modules_paths, $this->hints[$namespace]);

        return $this->findInPaths($view, $paths);
    }

} 