<?php

namespace Skimia\Themes\Facades;

use \Illuminate\Support\Facades\Facade;

class Assets extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Themes\Managers\Assets';
    }
}