<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 18/10/2014
 * Time: 19:04
 */
namespace Skimia\Themes\Managers;

use Skimia\Modules\Modules;
use Skimia\Themes\Facades\Theme as BaseTheme;
use Config;
class Assets{

    protected function getDefaultAsset($theme = false){
        if(!$theme)
            $theme = BaseTheme::getDefaultTheme();

        return $theme['assets']['default'];
    }

    protected function getCurrentAsset($theme = false){
        if(!$theme)
            $theme = BaseTheme::getDefaultTheme();
        if($theme['name'] != Config::get('skimia.themes::theme.theme'))
            return $this->getDefaultAsset($theme);

        return Config::get('skimia.themes::theme.assets');
    }

    public function getPrivateAssetDir($theme=false, $asset=false){
        if(!$theme)
            $theme = BaseTheme::getDefaultTheme();

        if(!$asset)
            $asset = $this->getCurrentAsset($theme);

        return rtrim($theme['path'],'/').'/assets/'.$asset;
    }

    protected function findPaths(){
        $theme = BaseTheme::getDefaultTheme();
        $asset = $this->getCurrentAsset($theme);

        $paths = [];
        $paths[] = $this->getPrivateAssetDir($theme,$asset);
        for( $i=0 ; $i<5 ; $i++ ){

            if($theme['parent']){
                $parent_theme = BaseTheme::getTheme($theme['parent']);
                $paths[] = $this->getPrivateAssetDir($parent_theme, $asset);
                $paths[] = $this->getPrivateAssetDir($parent_theme, $this->getDefaultAsset($parent_theme));
            }
            if($this->getDefaultAsset($theme) != $asset)
                $paths[] = $this->getPrivateAssetDir($theme, $this->getDefaultAsset($theme));

            if($theme['parent'])
                $theme = $parent_theme;
            else
                return $paths;

        }
        return $paths;
    }

    protected function findModulePaths($module_canonical){

        $module = Modules::getModuleInfo($module_canonical);

        $paths = [];

        $paths[] = $module['path'];
        for( $i=0 ; $i<5 ; $i++ ){
            if(isset($module['parent'])){
                $parent_module = Modules::getModuleInfo($module['parent']);
                $path[] = $parent_module['path'];
            }

            if(isset($module['parent']))
                $module = $parent_module;
            else
                return $paths;
        }

        return $paths;
    }

    public function getPublicAssetsWebDir($dir = false){
        if(!$dir)
            return \Request::server('REQUEST_URI').'assets';
        else{
            $dir = explode('/', trim(str_replace(Config::get('skimia.themes::filesystem.themes.dir.path'),'',$dir),DIRECTORY_SEPARATOR));


            $private_dir = Config::get('skimia.themes::filesystem.themes.dir.path').'/'.$dir[0].'/assets/'.$dir[2];
            $public_dir = public_path().'/assets/themes/'.$dir[0].'/'.$dir[2];

            if(!\File::exists($public_dir)) {
                if(!\File::exists(dirname($public_dir))){
                    \File::makeDirectory(dirname($public_dir),0777,true);
                }
                try{
                    symlink(
                        $private_dir,
                        $public_dir
                    );
                }catch(\ErrorException $e){
                    throw new \Exception('Erreur dans la création du lien symbolique, Vous devez peut-être executer votre serveur en administrateur',0,$e);
                }

            }
            return str_replace('index.php', '', \Request::server('SCRIPT_NAME')) .'assets/themes/'.$dir[0].'/'.$dir[2];
        }
    }

    public function getModulePublicAssetsWebDir($module){

            $module = Modules::getModuleInfo($module);

            $private_dir = $module['path'].'/resources/assets/public/';
            $public_dir = public_path().'/assets/modules/'.$module['canonical'];

            if(!\File::exists($public_dir) && \File::exists($private_dir)) {
                if(!\File::exists(dirname($public_dir))){
                    \File::makeDirectory(dirname($public_dir),0777,true);
                }
                try{
                    symlink(
                        $private_dir,
                        $public_dir
                    );
                }catch(\ErrorException $e){
                    //throw new \Exception('Erreur dans la création du lien symbolique, Vous devez peut-être executer votre serveur en administrateur',0,$e);
                }

            }
            return str_replace('index.php', '', \Request::server('SCRIPT_NAME')) .'assets/modules/'.$module['canonical'];
    }

    protected function findAsset($path, $module = false){
        //si module est defini check si le module existe
        $dirs = $this->findPaths();

        if($module){
            if(Modules::moduleState($module) < 1){
                throw new \Exception('le module ['.$module.'] n\'est pas activé ou n\'existe pas');
            }
            $module_dirs = $this->findModulePaths($module);
        }


        foreach($dirs as $dir){

            if($module){
                $module_dir = 'modules/'.str_replace('.','/',$module);

                if(\File::exists($dir.'/'.$module_dir.'/'.$path))
                    return $this->getPublicAssetsWebDir($dir, $module).'/'.$module_dir.'/'.$path;
            }else{
                if(\File::exists($dir.'/'.$path))
                    return $this->getPublicAssetsWebDir($dir, $module).'/'.$path;
            }

        }

        if($module){
            foreach($module_dirs as &$dir){

                if(\File::exists($dir.'/resources/assets/public/'.$path))
                    return $this->getModulePublicAssetsWebDir($module).'/'.$path;

                $dir = $dir.'/resources/assets/public/';
            }
        }

        return 'undefined';
        //TODO gestion message/exception
        throw new \Exception('l\'asset ['.$path.'] n\'est pas disponible dans les répertoires de recherches :'."\n".implode(', ',$dirs)."\n".($module?implode(', ',$module_dirs):''));
    }


    public function get($path, $module = false){
        return $this->findAsset($path,$module);
    }

    public function css($path, $module = false){
        return $this->get('css/'.$path,$module);
    }

    public function js($path, $module = false){
        return $this->get('js/'.$path,$module);
    }


    public function style($path, $module = false, $rel = 'stylesheet'){
        return ' <link href="'.$this->css($path,$module).'" rel="'.$rel.'">';
    }

    public function script($path, $module = false){
        return '<script src="' . $this->js($path,$module) . '"></script>';
    }

    public function favicon($path, $module = false){
        return '<link rel="icon" href="' . $this->get($path,$module) . '" />';
    }




}