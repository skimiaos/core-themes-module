<?php

namespace Skimia\Themes\Managers;

use Config;
use Illuminate\Support\Collection;

class Theme{

    protected $themes = [];

    protected $theme = '';

    public function __construct(){
        $this->theme = Config::get('skimia.themes::theme.theme');
        $this->themes = new Collection($this->themes);
    }

    public  function loadThemes(){
        $themes = \File::directories(Config::get('skimia.themes::filesystem.themes.dir.path'));

        foreach($themes as $theme){
            $this->loadTheme($theme);
        }
    }
    protected function defaultConfigArray(){

        return Config::get('skimia.themes::default.info');
    }
    protected function loadTheme($path){
        $config_file = $path.'/'.Config::get('skimia.themes::filesystem.theme.file.info');
        $theme_name = $this->getThemeCanonicalName($path);
        if(!\File::exists($config_file)){
            throw new \Exception('Theme invalide ou inexistant ['.$path.']');
        }

        $theme = array_merge($this->defaultConfigArray(), include $config_file);

        $theme['name'] = $theme_name;
        $theme['path'] = $path;
        $this->themes[$theme_name] = $theme;
    }

    protected function getThemeCanonicalName($path){
        return trim(str_replace(dirname($path),'',$path),DIRECTORY_SEPARATOR);
    }

    public function getDefaultTheme(){
        $theme_name = $this->theme;
        if(!isset($this->themes[$theme_name]))
            return current($this->themes);
        else
            return $this->themes[$theme_name];
    }

    public function setTheme($theme_name){
        if(!$this->themes->has($theme_name))
            throw new \Exception('Theme inexistant ['.$theme_name.']');
        $this->theme = $theme_name;
        return $this;
    }
    public function getTheme($name){
        return $this->themes[$name];
    }

    public function getThemes(){
        return $this->themes;
    }
    public function getIncludePaths()
    {

        $paths = [];

        $theme = $this->getDefaultTheme();

        if(isset($this->themes[$theme['name']]['paths']))
            return $this->themes[$theme['name']]['paths'];
        $paths[] = $theme['path'] . '/'.Config::get('skimia.themes::filesystem.theme.dir.views');

        while (isset($theme['parent']) && $theme['parent'] != false) {
            $theme = $this->getTheme($theme['parent']);
            $paths[] = $theme['path'] . '/'.Config::get('skimia.themes::filesystem.theme.dir.views');
        }
        $this->themes->get($theme['name'])['paths'] = $paths;
        return $paths;
    }

    public function registerModule($name){
        //TODO Ajouter le namespace du module dans le theme actuel;
    }
}