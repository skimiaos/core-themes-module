<?php

return [
    'name'        => 'Module Themes',
    'author'      => 'Skimia',
    'description' => 'Fournit la gestion des themes',
    'namespace'   => 'Skimia\\Themes',
    'require'     => ['skimia.modules']
];